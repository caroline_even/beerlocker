var beerdb = require('../models/beer');

exports.postBeers = function (req, res) {
    beerdb.insert(req.body.name, req.body.quantity, req.user.id, function (err, beer) {
        if (err) {
            res.send(err);
        }
        res.json({message: "Beer added to locker!", data: beer});
    });
};

exports.getBeers = function (req, res) {
    beerdb.getAll(req.user.id, function (err, beers) {
        if (err) {
            res.send(err);
        }
        res.json(beers);
    });
};

exports.getBeer = function (req, res) {
    beerdb.getBeer(req.user.id, req.params.id, function (err, beer) {
        if (err) {
            res.send(err);
        }
        res.json(beer);
    });
};

exports.putBeer = function (req, res) {
    beerdb.putBeer(req.user.id, req.params.id, req.body.quantity, function (err, beer) {
        if (err) {
            res.send(err);
        }
        res.json(beer);
    });
};

exports.deleteBeer = function (req, res) {
    beerdb.deleteBeer(req.user.id, req.params.id, function (err, beer) {
        if (err) {
            res.send(err);
        }
        res.json({message: 'deleted', data: beer});
    });
};