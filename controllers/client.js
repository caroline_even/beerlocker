var clientdb = require('../models/client');

exports.postClients = function (req, res) {
    clientdb.insert(req.body.name, req.body.secret, req.user.id, function (err, client) {
        if (err) {
            res.send(err);
        }
        res.json({message: "Client added to the locker!", data: client});
    });
};

exports.getClients = function (req, res) {
    clientdb.getClients(req.user.id, function (err, clients) {
        if (err) {
            res.send(err);
        }
        res.json(clients);
    });
};