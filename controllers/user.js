var userdb = require('../models/user');

exports.postUsers = function (req, res) {
    userdb.insert(req.body.username, req.body.password, function (err, user) {
        if (err)
            res.send(err);
        res.json({message: 'New beer drinker added to the locker room!', data: user});
    });
};

exports.getUsers = function (req, res) {
    userdb.getUsers(function (err, users) {
        if (err)
            res.send(err);
        res.json(users);
    });
};

exports.getUser = function (req, res) {
    userdb.getUser(req.params.id, function (err, user) {
        if (err)
            res.send(err);
        res.json(user);
    });
};