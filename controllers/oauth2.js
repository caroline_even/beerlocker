var oauth2orize = require('oauth2orize')
var userdb = require('../models/user');
var clientdb = require('../models/client');
var tokendb = require('../models/token');
var codedb = require('../models/code');

var server = oauth2orize.createServer();

// Register serialialization function
server.serializeClient(function (client, callback) {
    return callback(null, client.id);
});

// Register deserialization function
server.deserializeClient(function (id, callback) {
    clientdb.getClient(id, function (err, client) {
        if (err) {
            return callback(err);
        }
        return callback(null, client);
    });
});

// Register authorization code grant type
server.grant(oauth2orize.grant.code(function (client, redirectUri, user, ares, callback) {
    var value = uid(16);
    codedb.insert(value, redirectUri, user.id, client.id, function (err, code) {
        if (err) {
            return callback(err);
        }
        callback(null, value);
    });
}));

// Exchange authorization codes for access tokens
server.exchange(oauth2orize.exchange.code(function (client, codeValue, redirectUri, callback) {
    codedb.getCode(codeValue, function (err, code) {
        if (err) {
            return callback(err);
        }
        if (!code || code === undefined) {
            return callback(null, false);
        }
        if (client.id !== code.clientid) {
            return callback(null, false);
        }
        if (redirectUri !== code.redirecturi) {
            return callback(null, false);
        }

        codedb.delete(code.id, function (err, code) {
            if (err) {
                return callback(err);
            }
            var tokenValue = uid(256);
            tokendb.insert(tokenValue, code.userid, code.clientid, function (err, token) {
                if (err) {
                    return callback(err);
                }
                console.log('Granted token value ' + tokenValue);
                callback(null, token);
            });
        });
    });
}));

// User authorization endpoint
exports.authorization = [
    server.authorization(function (clientId, redirectUri, callback) {

        clientdb.getClient(clientId, function (err, client) {
            if (err) {
                return callback(err);
            }
            return callback(null, client, redirectUri);
        });
    }),
    function (req, res) {
        res.render('dialog', {transactionID: req.oauth2.transactionID, user: req.user, client: req.oauth2.client});
    }
];

function uid(len) {
    var buf = []
        , chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
        , charlen = chars.length;

    for (var i = 0; i < len; ++i) {
        buf.push(chars[getRandomInt(0, charlen - 1)]);
    }

    return buf.join('');
};

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// User decision endpoint
exports.decision = [
    server.decision()
];

// Application client token exchange endpoint
exports.token = [
    server.token(),
    server.errorHandler()
];