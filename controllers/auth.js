var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var BearerStrategy = require('passport-http-bearer').Strategy;
var userdb = require('../models/user');
var clientdb = require('../models/client');
var tokendb = require('../models/token');

passport.use(new BasicStrategy(function (username, password, cb) {
    userdb.getUserByName(username, function (err, user) {
        if (err) {
            return cb(err);
        }
        if (!user || user.length == 0) {
            return cb(null, false);
        }
        userdb.checkPassword(user.id, password, function (err, match) {
            if (err) {
                return cb(err);
            }
            if (!match) {
                return cb(null, false);
            }
            return cb(null, user);
        });
    });
}));

passport.use('client-basic', new BasicStrategy(function (id, secret, cb) {
    clientdb.getClient(id, function (err, client) {
        if (err) {
            return cb(err);
        }
        clientdb.checkSecret(client, secret, function (err, match) {
            if (err) {
                return cb(err);
            }
            if (!match) {
                return cb(null, false);
            }
            return cb(null, client);
        });
    });
}));

passport.use(new BearerStrategy(function (tokenValue, cb) {
    tokendb.getToken(tokenValue, function (err, token) {
        if (err) {
            return cb(err);
        }
        if (!token) {
            return cb(null, false);
        }
        userdb.getUser(token.userid, function (err, user) {
            if (err) {
                return cb(err);
            }
            if (!user) {
                return cb(null, false);
            }
            return cb(null, user, {scope: '*'});
        });
    })
}));

exports.isAuthenticated = passport.authenticate(['basic', 'bearer'], {session: false});
exports.isClientAuthenticated = passport.authenticate('client-basic', {session: false});
exports.isBearerAuthenticated = passport.authenticate('bearer', {session: false});