var express = require('express');
var bodyParser = require('body-parser');
var passport = require('passport');
var session = require('express-session');

var user = require('./models/user');
var beer = require('./models/beer');
var client = require('./models/client');
var code = require('./models/code');
var token = require('./models/token');

var app = express();

app.set('view engine', 'jade');

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(passport.initialize());

app.use(session({
    secret: 'Super Secret Session Key',
    saveUninitialized: true,
    resave: true
}))

passport.serializeUser(function (user, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
    user.getUser(id, function (err, user) {
        if (err) {
            cb(err);
        }
        else {
            cb(null, user);
        }
    });
});

var router = express.Router();

router.get('/', function (req, res) {
    res.json({message: "Try these URLS instead", data: "{/api/clients, /api/beers}"});
});

var authController = require('./controllers/auth');
var beerController = require('./controllers/beer');
var userController = require('./controllers/user');
var clientController = require('./controllers/client');
var oauth2Controller = require('./controllers/oauth2');

// Create endpoint handlers for /beers
router.route('/beers')
    .post(authController.isAuthenticated, beerController.postBeers)
    .get(authController.isAuthenticated, beerController.getBeers);

// Create endpoint handlers for /beers/:beer_id
router.route('/beers/:id')
    .get(authController.isAuthenticated, beerController.getBeer)
    .put(authController.isAuthenticated, beerController.putBeer)
    .delete(authController.isAuthenticated, beerController.deleteBeer);

router.route('/users')
    .post(authController.isAuthenticated, userController.postUsers)
    .get(authController.isAuthenticated, userController.getUsers);

router.route('/users/:id')
    .get(authController.isAuthenticated, userController.getUser);

router.route('/clients')
    .get(authController.isAuthenticated, clientController.getClients)
    .post(authController.isAuthenticated, clientController.postClients);

// Create endpoint handlers for oauth2 authorize
router.route('/oauth2/authorize')
    .get(authController.isAuthenticated, oauth2Controller.authorization)
    .post(authController.isAuthenticated, oauth2Controller.decision);

// Create endpoint handlers for oauth2 token
router.route('/oauth2/token')
    .post(authController.isClientAuthenticated, oauth2Controller.token);

app.use('/api', router);

var port = 3000;
app.listen(port);
console.log("Beerlocker started on port " + port + "!!!")