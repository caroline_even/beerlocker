# Beerlocker

## Overview

This code is written based on the tutorial provided by Scott Smith (see http://scottksmith.com/blog/2014/09/14/beer-locker-building-a-restful-api-with-node-digest/).
The mongoose database originally used has been replaced by postgres. Passwords, authorization code values, token values, and client secrets are all encrypted in the database.

## Running

The current setup assumes the existence of a postgres database named 'beerlocker'. It requires postgres running on port 5342. 

Simply run the javascript file server.js.