var db = require('./postgres').database;
var bcrypt = require('bcrypt-nodejs');

db.query('CREATE TABLE IF NOT EXISTS client(id SERIAL PRIMARY KEY, name VARCHAR(120) not null unique, secret VARCHAR(120) not null, userid integer not null REFERENCES users)')
    .then(function () {
        console.log('Created client table');
    })
    .catch(function (err) {
        console.log(err);
    });

function insert(name, secret, userid, cb) {
    var hash = bcrypt.hashSync(secret, bcrypt.genSaltSync(5));
    db.oneOrNone('INSERT INTO client(name, secret, userid) values($1, $2, $3) RETURNING *', [name, hash, userid])
        .then(function (client) {
            return cb(null, client);
        })
        .catch(function (err) {
            return cb(err);
        });
}

function getClients(userid, cb) {
    db.any('SELECT * FROM client WHERE userid=$1', [userid])
        .then(function (clients) {
            return cb(null, clients);
        })
        .catch(function (err) {
            return cb(err);
        });
}

function getClient(clientid, cb) {
    db.oneOrNone('SELECT * FROM client WHERE id=$1', [clientid])
        .then(function (client) {
            return cb(null, client);
        })
        .catch(function (err) {
            return cb(err);
        });
}

function checkSecret(client, secret, cb) {
    if (!client) {
        return cb(null, false);
    }
    bcrypt.compare(secret, client.secret, function (err, match) {
        if (err) {
            return cb(err);
        }
        cb(null, match);
    });
}

module.exports.insert = insert;
module.exports.getClients = getClients;
module.exports.getClient = getClient;
module.exports.checkSecret = checkSecret;