var db = require('./postgres').database;
var bcrypt = require('bcrypt-nodejs');

db.query('CREATE TABLE IF NOT EXISTS users(id SERIAL PRIMARY KEY, username VARCHAR(120) unique not null, password VARCHAR(120) not null)')
    .then(function () {
        insert('u1', 'password', function (err, user) {
            if (err) {
                console.log(err);
            }
            console.log('Inserted user ' + user.username);
        })
    })
    .catch(function (err) {
        console.log(err);
    });
;

function insert(name, password, cb) {
    var hashPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(5));
    db.oneOrNone('INSERT INTO users(username, password) values($1, $2) RETURNING *', [name, hashPassword])
        .then(function (user) {
            return cb(null, user);
        })
        .catch(function (err) {
            return cb(err);
        });
}

function getUser(userid, cb) {
    db.oneOrNone('SELECT * FROM users WHERE id=$1', userid)
        .then(function (user) {
            return cb(null, user);
        })
        .catch(function (err) {
            return cb(err);
        });
}

function getUserByName(username, cb) {
    db.oneOrNone('SELECT * FROM users WHERE username=$1', username)
        .then(function (user) {
            return cb(null, user);
        })
        .catch(function (err) {
            return cb(err);
        });
}

function getUsers(cb) {
    db.any('SELECT * FROM users')
        .then(function (users) {
            return cb(null, users);
        })
        .catch(function (err) {
            return cb(err);
        });
}

function checkPassword(userid, password, cb) {
    getUser(userid, function (err, user) {
        if (err) {
            return cb(err);
        }
        if (!user) {
            return cb(null, false);
        }
        bcrypt.compare(password, user.password, function (err, match) {
            if (err) {
                return cb(err);
            }
            cb(null, match);
        });
    });
}

module.exports.insert = insert;
module.exports.getUsers = getUsers;
module.exports.getUser = getUser;
module.exports.getUserByName = getUserByName;
module.exports.checkPassword = checkPassword;
