//https://www.npmjs.com/package/pg-promise

var options = {};
var pgp = require('pg-promise')(options);

var connection = {
    host: 'localhost',
    port: '5432',
    database: 'beerlocker'
};
exports.database = pgp(connection);