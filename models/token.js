var db = require('./postgres').database;
var bcrypt = require('bcrypt-nodejs');

db.query('CREATE TABLE IF NOT EXISTS token(id SERIAL PRIMARY KEY, value VARCHAR(120) not null, userid integer not null REFERENCES users, ' +
        'clientid integer not null REFERENCES client)')
    .then(function () {
        console.log('created token table');
    }).catch(function (err) {
    console.log(err);
});

exports.insert = function (value, userId, clientId, cb) {
    var hashValue = bcrypt.hashSync(value, bcrypt.genSaltSync(5));
    db.oneOrNone('INSERT INTO token(value, userid, clientid) values($1, $2, $3) RETURNING *', [hashValue, userId, clientId])
        .then(function (token) {
            return cb(null, token);
        })
        .catch(function (err) {
            return cb(err);
        });
};

exports.getToken = function (value, cb) {
    db.any('SELECT * FROM token')
        .then(function (tokens) {
            checkTokens(tokens, value, 0, function (err, matchingToken) {
                if (err) {
                    return cb(err);
                }
                if (!matchingToken) {
                    return cb(null, false);
                }
                return cb(null, matchingToken);
            });
        })
        .catch(function (err) {
            return cb(err);
        });
};

function checkTokens(tokens, value, index, cb) {
    if (!tokens || index >= tokens.length) {
        return cb(null, false);
    }
    checkToken(value, tokens[index], function (err, match) {
        if (!err && match) {
            return cb(null, tokens[index]);
        }
        return checkTokens(tokens, value, ++index, cb);
    });
}

function checkToken(value, token, cb) {
    if (!token) {
        return cb(null, false);
    }
    bcrypt.compare(value, token.value, function (err, match) {
        if (err) {
            return cb(err);
        }
        return cb(null, match);
    });
}