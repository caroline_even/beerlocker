var db = require('./postgres').database;
var bcrypt = require('bcrypt-nodejs');

db.query('CREATE TABLE IF NOT EXISTS code(id SERIAL PRIMARY KEY, value VARCHAR(120) not null, redirecturi VARCHAR(240) not null, userid integer not null REFERENCES users, ' +
        'clientid integer not null REFERENCES client)')
    .then(function () {
        console.log('created code table');
    })
    .catch(function (err) {
        console.log(err);
    });

exports.insert = function (value, redirectUri, userId, clientId, cb) {
    var hashValue = bcrypt.hashSync(value, bcrypt.genSaltSync(5));
    db.oneOrNone('INSERT INTO code(value, redirecturi, userid, clientid) values($1, $2, $3, $4) RETURNING *', [hashValue, redirectUri, userId, clientId])
        .then(function (code) {
            return cb(null, code);
        })
        .catch(function (err) {
            return cb(err);
        });
};

exports.delete = function (id, cb) {
    db.oneOrNone('DELETE FROM code WHERE id=$1 RETURNING *', id)
        .then(function (code) {
            return cb(null, code);
        })
        .catch(function (err) {
            return cb(err);
        });
};

exports.getCode = function (value, cb) {
    db.any('SELECT * FROM code')
        .then(function (codes) {
            checkCodes(codes, value, 0, function (err, matchingCode) {
                if (err) {
                    return cb(err);
                }
                if (!matchingCode) {
                    return cb(null, false);
                }
                return cb(null, matchingCode);
            });
        })
        .catch(function (err) {
            return cb(err);
        });
};

function checkCodes(codes, value, index, cb) {
    if (!codes || index >= codes.length) {
        return cb(null, false);
    }
    checkCode(value, codes[index], function (err, match) {
        if (!err && match) {
            return cb(null, codes[index]);
        }
        return checkCodes(codes, value, ++index, cb);
    });
}

function checkCode(value, code, cb) {
    if (!code) {
        return cb(null, false);
    }
    bcrypt.compare(value, code.value, function (err, match) {
        if (err) {
            return cb(err);
        }
        return cb(null, match);
    });
}