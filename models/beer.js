var db = require('./postgres').database;

db.query('CREATE TABLE IF NOT EXISTS beer(id SERIAL PRIMARY KEY, name VARCHAR(120) unique not null, quantity integer not null, userid integer not null REFERENCES users(id))')
    .then(function () {
        insert('Japanese', 50, 1, function (err, beer) {
            if (err) {
                console.log(err);
            }
            else {
                console.log('Inserted beer ' + beer.name);
            }
        });
        insert('American', 100, 1, function (err, beer) {
            if (err) {
                console.log(err);
            }
            else {
                console.log('Inserted beer ' + beer.name);
            }
        });
    })
    .catch(function (err) {
        console.log(err);
    });

function insert(name, quantity, userid, cb) {
    db.oneOrNone('INSERT INTO beer(name, quantity, userid) values($1, $2, $3) RETURNING *', [name, quantity, userid])
        .then(function (beer) {
            return cb(null, beer);
        })
        .catch(function (err) {
            return cb(err);
        });
}

function getAll(userid, cb) {
    db.any('SELECT * FROM beer WHERE userid=$1', userid)
        .then(function (beers) {
            return cb(null, beers);
        })
        .catch(function (err) {
            return cb(err);
        });
}

function getBeer(userid, id, cb) {
    db.oneOrNone('SELECT * FROM beer WHERE id=$1 AND userid=$2', [id, userid])
        .then(function (beer) {
            return cb(null, beer);
        })
        .catch(function (err) {
            return cb(err);
        });
}

function putBeer(userid, id, quantity, cb) {
    db.oneOrNone('UPDATE beer SET quantity=$1 WHERE id=$2 AND userid=$3 RETURNING *', [quantity, id, userid])
        .then(function (beer) {
            return cb(null, beer);
        })
        .catch(function (err) {
            return cb(err);
        });
}

function deleteBeer(userid, id, cb) {
    db.oneOrNone('DELETE FROM beer WHERE id=$1 AND userid=$2 RETURNING *', [id, userid])
        .then(function (beer) {
            return cb(null, beer);
        })
        .catch(function (err) {
            return cb(err);
        });
}

module.exports.insert = insert;
module.exports.getAll = getAll;
module.exports.getBeer = getBeer;
module.exports.putBeer = putBeer;
module.exports.deleteBeer = deleteBeer;
